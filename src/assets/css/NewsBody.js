import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
    NewsBodyContainer:{
        flex:1,
        width:'100%'
    },
    NewsBigLogo:{
        height: 250,
        width: '100%',
        resizeMode: 'cover',
    },
    NewsBody:{
        textAlign:'right',
        margin:8,
        fontFamily:'BYekan'
    }


});
