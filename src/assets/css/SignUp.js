import {Platform, StyleSheet} from 'react-native';
import {BtnHeight, BtnRadius, BtnWidth} from "./Styles";

export default styles = StyleSheet.create({
    SignupContainer:
        {
            width: '100%',
            alignItems: 'center',
        },
    SignupEeachRow: {
        width: '75%',
        margin: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        borderRadius: 150,
        borderColor: '#bbb',
        borderWidth: 1,
    },
    SingupBtnLabel: {
        fontFamily: 'BYekan',
        fontSize: 16,
        color:'#fff'
    },
    passwordInp: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    SignupInput: {
        height: BtnHeight,
        textAlign: 'right',
        width: '100%',
        fontFamily: 'BYekan',
        fontSize: 15,

        // ...Platform.select({
        //     ios: {},
        //     android: {
        //         elevation: 10
        //     }
        // })
    },
    SignupBtn: {
        marginTop:20,
        alignItems:'center',
        justifyContent:'center',
        width: BtnWidth,
        height: BtnHeight,
        borderRadius: BtnRadius,

    }
});
