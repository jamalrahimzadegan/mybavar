import { StyleSheet } from 'react-native';
import { LightText, BtnRadius, BoxBG } from "./Styles";

export default styles = StyleSheet.create({
    OldChatContainer:
    {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        width: '100%',
    },
    ChatContainer:
        { width: '100%', padding: 5, marginVertical: 6 },

    ContactBtn: {
        flexDirection: 'row',
        width: '95%',
        height: 60,
        padding: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: BtnRadius,
        backgroundColor: BoxBG,
        marginVertical: 5,
        alignSelf: 'center'
    },
    ContactTitle: {
        
        color: '#eee',
        fontSize: 15,
        fontFamily: 'BYekan',
        textAlign: 'right',
        flexShrink: 1,
    },
    LastMessage: {
        color: '#333',
        fontSize: 13,
        fontFamily: 'BYekan',
        textAlign: 'right',
        flexShrink: 1,
    },
    NewPmDot: {
        borderRadius: 150,
        height: 15,
        width: 15,
        borderColor: '#fff',
        borderWidth: 2.1,
        backgroundColor: 'lime'
    }

});
