import { StyleSheet } from 'react-native';
import {LightText, BtnRadius, BoxBG, DarkText, DarkBG} from "./Styles";

export default styles = StyleSheet.create({
    ChatPageContiner:
    {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        width: '100%',
    },
    ChatContainer:
        { width: '100%', padding: 5, marginVertical: 6 },
    ChatInpContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: DarkBG,
        width: '100%'
    },
    ChatInp: {
        fontFamily: 'BYekan',
        color: DarkText,
        width: '90%',
        paddingHorizontal: 10
    },
    SendBtn: {
        width: '10%',
        alignItems: 'center',
        justifyContent: 'center',

    },
    
RecieverBubble:{
    alignSelf: 'flex-start',
    maxWidth: '90%',
    marginVertical: 5,
    flex: 1,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomRightRadius: 15,
    backgroundColor: '#0984e3',
    paddingHorizontal: 13,
    paddingVertical: 5,
},
SenderBubble: {
        
        alignSelf: 'flex-end',
        maxWidth: '90%',
        marginVertical: 5,
        flex: 1,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        backgroundColor: '#341f97',
        paddingHorizontal: 13,
        paddingVertical: 5,
    },
    MessageText: {
        color: '#fff',
        fontSize: 15,
        fontFamily: 'BYekan',
        textAlign: 'right'
    },
    // ------Province----------------------------------------------------------------------------------------
    ProvinceListContainer: {
        flex: 1
    },
    ProvinceBtn: {
        width: '90%',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: BtnRadius,
        backgroundColor: BoxBG,
        margin: 5,
        alignSelf: 'center'
    }
        // ------Chat peers----------------------------------------------------------------------------------------

});
