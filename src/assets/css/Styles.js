import {StyleSheet} from 'react-native';
import React from 'react';

const Styles = StyleSheet.create({

  DropDownTitle: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'BYekan',
  },
  DropDownBody: {
    color: '#fff',
    fontSize: 14,
    fontFamily: 'BYekan',
  },
  EmptyListText:{
    fontSize: 15,
    fontFamily: 'BYekan',
  }
});
export default Styles;



export const DarkBG = '#121212';
export const DarkText = '#eee';
export const LightBG = '#fff';
export const LightText = '#333';
export const BtnRadius = 5;
export const BtnWidth = '50%';
export const BtnHeight = 45;


export const BoxBG = '#46b5d1';
export const PickerBg = [32, 18, 82, .2];
export const PickerToolBarBg = [32, 18, 82, 1];
export const PickerTitleColor = [255, 255, 255, 1];
export const PickerConfirmBtnColor = [255, 255, 255, 1];
export const PickerCancelBtnColor = [255, 255, 255, 1];
export const PickerFontColor = [0, 0, 0, 1];
