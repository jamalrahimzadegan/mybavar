import {StyleSheet} from 'react-native';

export default  styles = StyleSheet.create({
    HeaderContainer: {
        width: '100%',
        height: 45,
        justifyContent: 'center',
        ...Platform.select({
            ios: {
                paddingTop: 25
            }
        }),
    },
    HeaderIconsContainer: {
        width: '96%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        alignSelf: 'center'
    }
    ,
    TextContainer: {
        flexShrink: 1,
        width: '96%',
        height: 42,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end'
    },
    TitleStyle: {
        marginVertical: 1,
        flexShrink: 1,
        color: '#fff',
        fontFamily: 'BYekan',
        fontSize: 16,
    }
});
