import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
    SplashContainer:
        {
            flex: 1,
            justifyContent: 'space-around',
            alignItems: 'center',
            width: '100%',
        },
    SplashLogo: {
        height: 200,
        width: 200,
        resizeMode: 'contain'
    },
    SplashTitle: {
        color: '#fff',
        fontFamily: 'BYekan',
        fontSize: 25
    }
});
