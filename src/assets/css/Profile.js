import { StyleSheet } from 'react-native';
import { BtnHeight, BtnRadius, BtnWidth } from "./Styles";

export default styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        width: '100%',
    },
    ProfileAvatar: {
        width: '100%',
        height: 200,
        resizeMode: 'contain',
        borderRadius: 40 / 2
    },
    EachProfileRow: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    ProfileLabel: {
        fontFamily: 'BYekan',
    },
    ProfileInput: {
        textAlign:'center',
        borderRadius:BtnRadius,
        paddingHorizontal: 10,
        backgroundColor: '#ddd',
        width: '95%',
        alignSelf: 'center',
        color: '#333'
    },
    UpdateProfileBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',

        margin: 10,
        height: BtnHeight,
        borderRadius: BtnRadius,
        width: BtnWidth
    },
    UpdateProfileLabel: {
        fontFamily: 'BYekan',
        color: '#fff'
    }
});
