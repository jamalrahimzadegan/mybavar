import {Dimensions, StyleSheet} from 'react-native';
import {BoxBG, BtnRadius} from "./Styles";

export default styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        width: '100%',
    },
    MainPageCover: {
        height:230,
        alignSelf: 'center',
        width: '100%',
        resizeMode: 'stretch',
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        // borderBottomLeftRadius: 10,
        // borderBottomRightRadius: 10,
        marginBottom: 11,
    },
    MainPageLogo: {
        // borderWidth: .1,
        // borderColor: '#fff',
        borderRadius: 200,
        height: 140,
        width: 140,
        resizeMode: 'cover',
    },
    MainTitleContainer:
        {height: '100%', alignItems: 'flex-end', justifyContent: 'center'},
    DetailContainer:
        {flexDirection: 'row', alignItems: 'center'},
    MainPageDiv: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignSelf: 'center',
        marginHorizontal: 5
    },
    MainPageBtnContainer: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    FileBox: {
        borderTopRightRadius:150,
        borderBottomRightRadius:150,
        borderTopLeftRadius:50,
        borderBottomLeftRadius:50,
        flex: 1,
        flexDirection: 'row-reverse',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: BoxBG,
        marginVertical: 4,
        width: '96%',
        height: 60
    },
    FileTitle: {
        lineHeight: 24,
        color: '#333',
        fontFamily: 'BYekan',
        fontSize: 16,
        textAlign: 'right',
        flexShrink: 1
    },
    FileLogo: {
        zIndex: 100,
        height: 60,
        width: 60,
        marginLeft: 10,
        resizeMode: 'cover',
        borderRadius: 150
    },
    NewChatBtn: {
        marginVertical: 5,
        padding: 5,
        borderRadius: 150,
        backgroundColor: BoxBG,
        borderColor: '#fff',
        borderWidth: 2.5,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
