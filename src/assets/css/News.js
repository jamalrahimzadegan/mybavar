import { StyleSheet } from 'react-native';
import { BoxBG, BtnRadius } from "./Styles";

export default styles = StyleSheet.create({
    MainContainer:
    {
        flex: 1,
        width: '100%',
        // justifyContent:'center',
        // alignItems:'center'
    },
    MainPageDiv: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignSelf: 'center',
        marginHorizontal: 5
    },

    NewsBoxes: {
        flexDirection: 'row-reverse',
        alignSelf: 'center',
        // justifyContent:'center',
        backgroundColor: BoxBG,
        padding: 5,
        width: '100%',
        height: 90
    },
    NewsTitle: {
        lineHeight: 24,
        alignSelf: 'center',
        color: '#fff',
        fontFamily: 'BYekan',
        // width: '76%',
        fontSize: 16,
        textAlign: 'right',
        flexShrink: 1
        // backgroundColor: 'red',
    },
    NewsLogo: {
        height: 80,
        width: 80,
        marginLeft: 10,
        resizeMode: 'cover',
    },
    NewChatBtn: {
        marginVertical:5,
        padding: 5,
        borderRadius: BtnRadius,
        backgroundColor: BoxBG,
        width: 55,
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
    }



});
