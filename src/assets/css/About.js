import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
    AboutContainer:{
        flex:1,
        width:'100%'
    },
   
    AboutTitle:{
        textAlign:'right',
        margin:8,
        fontSize:19,
        fontFamily:'BYekan'
    },
    AboutBody:{
        textAlign:'right',
        margin:8,
        fontFamily:'BYekan'
    },


});
