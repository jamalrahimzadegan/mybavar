import {StyleSheet, Dimensions} from 'react-native';
import {Colors} from './Styles';

export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
    },
    MainView2: {
        alignItems: 'flex-end',
        padding: 10
    },
    DrawerTopSegment: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        // justifyContent:'space-between',
        alignSelf: 'flex-start',
        width: '100%'
    },
    DrawerNameToggleBtn: {},
    Avatar: {
        borderRadius: 150,
        height: 70,
        width: 70,
        resizeMode: 'contain',
        marginLeft: 20
    },
    DrawerLogoApp: {
        margin:20,
        alignSelf: 'center',
        height: 100,
        width: 100,
        resizeMode: 'contain',
    },
    DrawerRow: {
        width: '100%',
        justifyContent: 'flex-end',
        padding: 10,
        backgroundColor: 'rgba(255,255,255,.05)',
        flexDirection: 'row',
        alignItems: 'center'
    },
    ToggleText: {
        marginBottom: 5,
        fontFamily: 'BYekan',
    }

});
