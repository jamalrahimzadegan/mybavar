import {Dimensions, StyleSheet} from 'react-native';
import {BoxBG, BtnHeight, BtnRadius} from "./Styles";

export default styles = StyleSheet.create({
    ReminderContainer: {
        flex: 1,
        width: '100%',
    },
    ReminderBtn: {
        height: 100,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 4,
        backgroundColor: BoxBG,
        width: '97%',
        alignSelf: 'center',
        borderRadius: BtnRadius,
        padding: 10
    },
    ReminderIcons: {
        // backgroundColor:'red',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '100%',
        width: '10%'
    },
    ReminderTextContainer: {
        justifyContent: 'space-between',
        // backgroundColor: 'lime',
        height: '100%',
        paddingLeft: 8,
        borderLeftWidth: 1,
        borderColor: '#fff',
        width: '90%',
    },
    ReminderTitle: {
        flexShrink: 1,
        textAlign: 'right',
        color: '#fff',
        fontFamily: 'BYekan'
    },
    ReminderTitleInp: {
        paddingHorizontal: 10,
        borderWidth: 1,
        borderRadius: 150,
        height: BtnHeight,
        textAlign: 'center',
        width: '96%',
        alignSelf: 'center',
        fontFamily: 'BYekan',
        fontSize: 15,

    },
    ReminderTime: {
        fontFamily: 'BYekan',
        textAlign: 'center',
        margin: 10,
        fontSize: 20,
        borderRadius: 150,
        height: BtnHeight,
        alignSelf: 'center',
        // padding:,
    },
    EditReminderTime: {
        flexDirection: 'row',
        overflow: 'hidden',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 10,
    },
    RemUpBtn: {
        width: '50%',
        height: BtnHeight,
        borderRadius: BtnRadius,
        backgroundColor: BoxBG,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        margin:20
    },
    AddRemBtn:{
        backgroundColor:BoxBG,
        position:'absolute',
        right:10,
        bottom:10,
        borderRadius:150,
        zIndex:100
    }

});
