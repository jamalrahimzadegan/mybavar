import {StyleSheet} from 'react-native';
import {BoxBG, BtnRadius, DarkBG} from "./Styles";

export default styles = StyleSheet.create({
    PlayerContainer:
        {
            flex: 1,
            width: '100%',
            backgroundColor: 'black',
            // justifyContent:'center',
            // alignItems:'center'
        },
    TimesFont: {
        backgroundColor: 'rgba(255,255,255,.07)',
        borderRadius: 150,
        paddingHorizontal:8,
        color: '#fff',
        fontFamily: 'BYekan'
    },
    PlayerControls: {
        height: 100,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    CtrlBarContainer: {
        backgroundColor: 'black',
        bottom: 0,
        position: 'absolute',
        alignItems: 'center'
    },
    SeekBarContainer: {
        marginVertical: 5,
        height: 4,
        width: '100%',
        backgroundColor: '#ccc'
    },
    SeekBar: {
        backgroundColor: BoxBG,
        height: '100%',

    },
    CtrlBtnContainer: {
        width: 55,
        height: 55,
        // alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,.2)',
        borderRadius: 150,
        padding: 12
    },
    Loader:
        {flex: 1, backgroundColor: 'rgba(1,1,1,.7)', position: 'absolute', top: 0, bottom: 0, left: 0, right: 0},
    HideCtrlBtn:
        {position: 'absolute', zIndex: 100, top: 25, right: 15}


});
