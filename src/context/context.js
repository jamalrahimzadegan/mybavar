import React, {useState, useEffect} from 'react';
import {AsyncStorage} from 'react-native';


export const Context = React.createContext();


export function ContextProvider(props) {
    const [id, setId] = useState();
    const [role, setRole] = useState();
    const [username, setUsername] = useState();
    const [theme, setTheme] = useState();

    AsyncStorage.multiGet(['theme', 'id', 'role'])
        .then((x) => {
            setId(x[1][1])
            setRole(x[2][1])
            if (x[0][1] == null) {
                AsyncStorage.setItem('theme', 'light');
                setTheme('light')
            } else if (x[0][1] != null) {
                setTheme(x[0][1])
            }
            // console.warn('role is : ',(x[2][1]))
        });

    return (
        <Context.Provider
            value={{
                theme,
                id,
                role,
                username,
                setUsername: (name) => setUsername(name),
                setId: (id) => setId(id),
                setRole: (role) => setRole(role),
                setTheme: (x) => setTheme(x),
            }}>
            {props.children}
        </Context.Provider>
    );
}
