import {Component} from "react";
import React from "react";




export default class Connect extends Component {
    constructor(props) {
        super(props);
    }

    static PostReq(action, Params = {}) {
        return fetch(action, {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, body: JSON.stringify(Params)
        }).then((response) => response.json())
    }



    //--------------FormatNumber-------------------------------------------------------------------------------------------------
    static FormatNumber(price) {
        // return price
        return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

}
