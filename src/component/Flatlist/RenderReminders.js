import {Text, Image, TouchableOpacity, View, Alert} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/Reminders';
import React, {useEffect, useState, useContext} from 'react';
import Icon from "react-native-vector-icons/AntDesign";
import Connect from "../../core/Connect";
import Host from "../../core/Host";


let PushNotification = require("react-native-push-notification");


export default function RenderReminders(props) {
    let item = props.item.item;

    //---------Managing Reminders---------------------------------------------------------------------------------
    const SetReminders = () => {
        // console.warn('SetRimener is working')
        let GMTdiffer = Math.abs((new Date()).getTimezoneOffset()) * 60; // اختلاف زمانی ایران و گرینویچ به ثانیه
        let ReminderIranTimeSec = (`${item.time}:00`.split(':').reduce((acc, time) => (60 * acc) + +time));// زمان ریماندر به وقت ایران و ثانیه
        let ReminderInSecGMT = Number(ReminderIranTimeSec) - Number(GMTdiffer); //زمان ریماندر به ثانیه به وقت گرینویچ
        let ReminderTimeInGMT = new Date(Number(ReminderInSecGMT) * 1000).toISOString().substr(11, 8);
        PushNotification.localNotificationSchedule({
            message: `${item.text}`,
            repeatType: 'day',
            date: new Date(`${item.date}T${ReminderTimeInGMT}.111Z`) //original
        });
    };
    //---------Delete Reminder---------------------------------------------------------------------------------
    const Delete = (RID, userID) => {
        Alert.alert(
            '', 'آیا مایل به حذف این یادآور می باشید؟',
            [
                {
                    text: 'خیر',
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: 'بله', onPress: () => {
                        Connect.PostReq(Host.Link('deletereminder'), {
                            userId: parseInt(userID),
                            id: parseInt(RID)
                        }).then(res => {
                            if (res) {
                                PushNotification.cancelAllLocalNotifications();
                                alert('یادآور با باموفقیت حذف شد')
                                props.GetReminders(userID);
                            }
                        }).catch((e) => null);
                    }
                },
            ],
            {cancelable: false},
        );
    };
    useEffect(() => {
        SetReminders()
    }, [item.time, item.length]);
    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.ReminderBtn, {}]}>
                    <View style={styles.ReminderTextContainer}>
                        <Text style={styles.ReminderTitle}>{item.text}</Text>
                        <Text style={styles.ReminderTitle}>زمان: هر روز رأس ساعت {item.time}</Text>
                    </View>
                    <View style={styles.ReminderIcons}>
                        <Icon onPress={() => Delete(item.id, context.id)} name={'delete'} color={'#fff'} size={27}/>
                        <Icon onPress={() => props.navigation.navigate('EditReminders', {
                            item: item,
                            GetReminders: (id) => props.GetReminders(id),
                            SetReminders: () => SetReminders(),
                        })} name={'edit'} color={'#fff'} size={27}/>
                    </View>
                </View>
            )}
        </Context.Consumer>
    );
};


