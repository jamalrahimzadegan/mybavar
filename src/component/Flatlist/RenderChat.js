import React, { useState } from 'react';
import {
    View,
    Text,

} from 'react-native';
import { Context } from '../../context/context';
import styles from './../../assets/css/Chat';


function RenderChat(props) {
    let Chat = props.item.item;
    // console.warn('chat: ', props)
    return (
        <Context.Consumer>
            {(context) => (
                <View>
                    {
                        context.id == Chat.senderId ?
                            <View style={[styles.SenderBubble]}>
                                <Text style={[styles.MessageText, {}]}>{Chat.content}</Text>
                            </View> :
                            <View style={[styles.RecieverBubble]}>
                                <Text style={[styles.MessageText, {}]}>{Chat.content}</Text>
                            </View>
                    }
                </View>

            )}
        </Context.Consumer>
    );
}
export default RenderChat;




