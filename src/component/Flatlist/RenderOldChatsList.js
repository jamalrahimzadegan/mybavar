import React, {useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/ChatList';


function RenderOldChatsList(props) {
    let Item = props.item.item;
    useEffect(() => {
        props.GetChatList()
    }, []);
    // console.warn('userId: ', Item.userId)
    return (
        <Context.Consumer>
            {(context) => (
                <TouchableOpacity onPress={() => props.navigation.navigate('Chat', {
                    PeerName: Item.mfullname,
                    ChatType: Item.id,
                    usermID: Item.usermId,
                    userID: Item.userId
                })}
                    // <TouchableOpacity onPress={() => props.navigation.navigate('Chat', { PeerName: Item.mfullname, ChatType: Item.id, usermID: Item.usermId })}
                                  style={styles.ContactBtn}>
                    {
                        Item.count != 0 ?
                            <View style={styles.NewPmDot}/>
                            : <View/>
                    }
                    <View style={{width: '88%'}}>
                        <Text numberOfLines={1}
                              style={styles.ContactTitle}>{context.role == 'user' ? Item.mfullname : Item.ufullname}</Text>
                        <Text numberOfLines={1}
                              style={[styles.LastMessage, {fontSize: Item.count != 0 ? 14 : 13}]}>{Item.content}</Text>

                    </View>
                </TouchableOpacity>

            )}
        </Context.Consumer>
    );

}

export default RenderOldChatsList;




