import React from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/Main';
import Host from "../../core/Host";


export default function RenderFiles(props) {
    let item = props.item.item;
    // console.warn(item)
    // console.warn(item.file)
    let FileType = item.file.slice((item.file.lastIndexOf(".") - 1 >>> 0) + 2);
    const Navigate = () => {
        switch (FileType) {
            case "mp4":
                props.navigation.navigate('Player', {fileName: item, Type: FileType});
                break;
            case "mp3":
                props.navigation.navigate('Player', {fileName: item, Type: FileType});
                break;
            case "pdf":
                props.navigation.navigate('ShowPdf', {PdfName: item});
                break;
            default:
        }
    };

    return (
        <Context.Consumer>
            {(context) => (
                <TouchableOpacity
                    onPress={Navigate}
                    style={[styles.FileBox, {}]}>
                    {
                        item.img ?
                            <Image source={{uri: Host.Media(item.img)}}
                                   style={styles.FileLogo}/>
                            :
                            <Image source={require('./../../assets/Images/newsLogo.png')} style={styles.NewsLogo}/>
                    }
                    <View style={styles.MainTitleContainer}>
                        <Text numberOfLines={2} style={[styles.FileTitle, {color:'#fff'}]}>{item.name}</Text>
                        <View style={styles.DetailContainer}>
                            <Text numberOfLines={1} style={[styles.FileTitle, {fontSize:12}]}>نوع: {FileType == 'mp4'?'ویدیویی':FileType == 'mp3'?'صوتی':FileType == 'pdf'?'متنی':'نامشخص'}</Text>
                            <Text numberOfLines={2} style={[styles.FileTitle, {fontSize:12}]}>نویسنده: {item.author}         </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )}
        </Context.Consumer>
    );
};




