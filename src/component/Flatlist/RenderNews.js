import React from 'react';
import {
    Image,
    View, Text, TouchableOpacity
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/News';
import Host from "../../core/Host";


function RenderNews(props) {
    let item = props.item.item; 
    // console.warn(item)
    return (
        <Context.Consumer>
            {(context) => (
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('NewsBody', {news: item})}
                    style={[styles.NewsBoxes, {}]}>
                    {
                        item.image ?
                            <Image source={{uri: Host.Media(item.image)}}
                                   style={styles.NewsLogo}/>
                            :
                            <Image source={require('./../../assets/Images/newsLogo.png')} style={styles.NewsLogo}/>
                    }
                    <Text numberOfLines={2}  style={[styles.NewsTitle, {}]}>{item.title}</Text>
                </TouchableOpacity>
            )}
        </Context.Consumer>
    );
}

export default RenderNews;




