﻿import {ActivityIndicator, AsyncStorage, Image, ImageBackground, Text, StatusBar} from 'react-native';
import {Context} from '../context/context';
import styles from './../assets/css/Splash';
import {BoxBG} from "../assets/css/Styles";
import React, {useEffect, useState,useContext} from 'react';

export default function Splash({navigation}) {

    let context = useContext(Context);
    const [counter, setCounter] = useState(0);
    const Navigate = async () => {
                // navigation.replace('Reminders')

        if (counter != 0) {
            if (context.id == undefined || context.id == 'logout') {
                navigation.replace('Login')
            } else {
                if (context.role == 'user') {
                    navigation.replace('Main')
                } else if (context.role == 'moshaver') {
                    navigation.replace('OldChatList')
                }
            }
        } else {
        }
    };

    useEffect(() => {
        setTimeout(() => {
            Navigate()
        }, 1000)
    }, [context]);
    return (
        <Context.Consumer>
            {(context) => (
                <ImageBackground
                    onPress={setCounter(1)}
                    blurRadius={5}
                    source={require('./../assets/Images/SplashBack.jpg')}
                    style={[styles.SplashContainer, {}]}>
                    <Image
                        onPress={setCounter(2)}
                        source={require('./../assets/Images/Logo.png')}
                        style={[styles.SplashLogo]}/>
                    <StatusBar hidden={true}/>
                    <ActivityIndicator color={BoxBG} size={32}/>
                </ImageBackground>
            )}
        </Context.Consumer>
    );


}




