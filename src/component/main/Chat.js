import React, {useState, useContext, useEffect, useRef} from 'react';
import {
    ScrollView,
    View,
    ImageBackground,
    FlatList,
    TouchableOpacity,
    TextInput,
    ActivityIndicator
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Chat';
import Host from "../../core/Host";
import Connect from "../../core/Connect";
import Styles, {BoxBG, DarkText, LightBG, LightText} from "../../assets/css/Styles";
import Header from "./Header";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import RenderChat from "../Flatlist/RenderChat";
import Empty from "./Empty";


export default function Chat({navigation}) {
    const [loading, setLoading] = useState();
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState();
    const [scrollPage, setScroll] = useState(false);

    //---------------------------------------------------------------------------------------------------------------------------------------
    const context = useContext(Context);
    //---------------------------------------------------------------------------------------------------------------------------------------
    const GetMessage = () => {
        Connect.PostReq(Host.Link('detailticket'), {
            usermId: parseInt(navigation.getParam('usermID')),
            userId: navigation.getParam('ChatType') != -1 ? parseInt(navigation.getParam('userID')) : parseInt(context.id),
        }).then(res => {
            setMessages(res.tickets)
        }).catch((e) => {
        });
    };
    //-----------send ticket to the server----------------------------------------------------------------------------------------------------------------------------
    const SendMessage = () => {
        if (message !== '') {
            setLoading(true)
            Connect.PostReq(Host.Link('sendticket'), {
                ticketId: parseInt(navigation.getParam('ChatType')),
                content: message,
                file: null,
                type: null,
                userId: parseInt(context.id),
                usermId: parseInt(navigation.getParam('usermID')),
                // username: username,
            }).then(res => {
                setLoading(false);
                if (res.result) {
                    setMessage('')
                    setScroll(true)
                    // console.warn(res)
                    GetMessage()
                    // setMessages()
                }
            }).catch((e) => {
                setLoading(false);
                alert('پیام ارسال نشد دوباره تلاش کنید')
            });
        } else {
        }
    };
    //--------------------------------------------------------------------------------------------------------------------------------------
    useEffect(() => {
        GetMessage();
        const interval = setInterval(() => GetMessage(), 15000)
        return () => clearInterval(interval);
    }, []);


    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.ChatPageContiner, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={navigation}/>
                    <ScrollView

                        showsVerticalScrollIndicator={false}
                        style={{width: '100%'}}
                        // ref={ref => scrollView = ref}
                        // onContentSizeChange={(contentWidth, contentHeight) => {
                        //     if (scrollPage) {
                        //         // alert('scrool')
                        //         scrollView.scrollToEnd({animated: false});
                        //         setTimeout(() => {
                        //             setScroll(false)
                        //         }, 1000)
                        //     }
                        // }}
                    >

                        <ImageBackground style={{flex: 1, paddingHorizontal: 5, paddingVertical: 10}}
                                         source={require('./../../assets/Images/chatback.jpg')}>
                            <Empty onPress={() => ScrollToEnd()}
                                   Message={` گفتگو با ${navigation.getParam('PeerName')}`}/>
                            <FlatList
                                style={styles.ChatContainer}
                                data={messages}
                                ListEmptyComponent={() => <Empty Message={'پیامی وجود ندارد'}/>}
                                keyExtractor={(item, index) => index}
                                renderItem={(item) => <RenderChat item={item}/>}
                            />
                        </ImageBackground>


                    </ScrollView>
                    <View style={styles.ChatInpContainer}>
                        <TextInput
                            onChangeText={(x) => setMessage(x)}
                            style={[styles.ChatInp]}
                            value={message}
                            multiline={true}
                            placeholder={' '}
                        />
                        <TouchableOpacity style={styles.SendBtn}>
                            {
                                loading ?
                                    <ActivityIndicator size={25} color={BoxBG}/>
                                    :
                                    <Icon onPress={SendMessage} color={LightBG} name={'send-circle'} size={40}/>

                            }
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </Context.Consumer>
    );
};





