import React, {useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/Chat';
import Host from "../../core/Host";
import Connect from "../../core/Connect";
import Header from "./Header";
import Empty from "./Empty";


export default function ChatPeers(props, {navigation}) {
    const [peers, setPeers] = useState([]);
    //--------------------------------------------------------------------------------------------------------------------------------------
    const GetPeers = () => {
        Connect.PostReq(Host.Link('moshaveran'), {
            provinceId: parseInt(props.navigation.getParam('ProID'))
        }).then(res => {
            setPeers(res.moshaveran)
            // console.warn(res.moshaveran)
        }).catch((e) => {
        });
    };

    //---------------------------------------------------------------------------------------------------------------------------------------
    const Peers = (item) => {
        let Item = item.item.item;
        // console.warn(Item)
        return (
            //  -1 is a new chat with this user
            <TouchableOpacity onPress={() => props.navigation.navigate('Chat', {
                PeerName: Item.fullname,
                ChatType: -1,
                usermID: Item.userId
            })} style={styles.ProvinceBtn}>
                <Text style={styles.MessageText}>{Item.fullname}</Text>
            </TouchableOpacity>
        )
    }
    //------------province render --------------------------------------------------------------------------------------------------------------------------
    useEffect(() => {
        GetPeers()
    }, []);
    return (
        <Context.Consumer>
            {(context) => (
                <View
                    style={[styles.ProvinceListContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={props.navigation}/>
                    <FlatList
                        style={styles.ChatContainer}
                        data={peers}
                        ListEmptyComponent={() => <Empty Message={'مشاوری وجود ندارد'}/>}
                        keyExtractor={(item, index) => index}
                        renderItem={(item) => <Peers item={item} navigation={navigation}/>}
                    />
                </View>
            )}
        </Context.Consumer>
    );

};





