import React, {useEffect, useState, useContext} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity, ActivityIndicator
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Profile';
import Header from "./Header";
import Connect from "../../core/Connect";
import Host from "../../core/Host";
import Styles, {DarkText, LightText} from "../../assets/css/Styles";


export default function Profile({navigation}) {
    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [mobile, setMobile] = useState();
    const [fullname, setFullname] = useState();
    const [address, setAddress] = useState();
    const [hidePass, HidePass] = useState(true);
    const [loading, setLoading] = useState(false);
    //----get the context------------------------------------------------------------------------------------------------
    const context = useContext(Context);
    //----------Get Profile Detials---------------------------------------------------------------------------------------
    const GetProfile = (id) => {
        Connect.PostReq(Host.Link('getprofile'), {
            id: parseInt(id),
        }).then(res => {
            let Res = res.profile;
            context.setUsername(res.user.username)
            setUsername(res.user.username);
            setEmail(res.user.email);
            setPassword(null);
            setMobile(Res.mobile);
            setFullname(Res.fullname);
            setAddress(Res.address);

        }).catch((e) => null);
    };
    //----------UpdateProfile---------------------------------------------------------------------------------------
    const UpdateProfile = () => {
        setLoading(true)
        // console.warn('id:', context.id, 'FUllName:', fullname, 'usernamae:', username, 'mobile:', mobile, 'add:', address, 'email:', email, 'pas:', password)
        Connect.PostReq(Host.Link('profile'), {
            id: parseInt(context.id),
            username: username,
            email: email,
            password: '123456',
            fullname: fullname,
            mobile: mobile,
            address: address
        }).then(res => {
            if (res.result) {
                GetProfile
                alert('ثبت اطلاعات با موفقیت انجام شد')
            }
            setLoading(false)
            // console.warn(res)
        }).catch((e) => {
            alert('ثبت اطلاعات انجام نشد لطفا دوباره تلاش کنید')
            setLoading(false)
        });
    };

    useEffect(() => {
        // console.warn('id: ',context.id)
        GetProfile(context.id)
    }, []);


    return (
        <Context.Consumer>
            {(context) => (
                <View style={{flex: 1}}>
                    <ScrollView
                        style={[styles.MainContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                        <Header navigation={navigation}/>
                        {/* {
                            0 ?
                                <Image source={{ uri: Host.Media(null) }}
                                    style={styles.ProfileAvatar} />
                                :
                                <Image source={require('./../../assets/Images/avatar.png')} style={styles.ProfileAvatar} />
                        }<Seperator marginVertical={10} /> */}
                        {/*---------Fullname--------------------------------------------------------------------------------------*/}
                        <View style={styles.EachProfileRow}>
                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                نام کامل
                            </Text>
                            <TextInput
                                onChangeText={(x) => setFullname(x)}
                                style={[styles.ProfileInput]}
                                value={fullname}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        {/*---------username--------------------------------------------------------------------------------------*/}
                        <View style={styles.EachProfileRow}>
                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                نام کاربری
                            </Text>
                            <TextInput
                                onChangeText={(x) => setUsername(x)}
                                style={[styles.ProfileInput]}
                                value={username}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        <View style={styles.EachProfileRow}>

                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                رمز عبور
                            </Text>
                            <TextInput
                                onChangeText={(x) => setPassword(x)}
                                style={[styles.ProfileInput]}
                                value={password}
                                // secureTextEntry={hidePass}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        {/*---------email--------------------------------------------------------------------------------------*/}
                        <View style={styles.EachProfileRow}>
                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                ایمیل
                            </Text>
                            <TextInput
                                keyboardType={'email-address'}
                                onChangeText={(x) => setEmail(x)}
                                style={[styles.ProfileInput]}
                                value={email}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        {/*---------mobile--------------------------------------------------------------------------------------*/}
                        <View style={styles.EachProfileRow}>
                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                شماره موبایل </Text>
                            <TextInput
                                keyboardType={'numeric'}
                                maxLength={11}
                                onChangeText={(x) => setMobile(x)}
                                style={[styles.ProfileInput]}
                                value={mobile}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        {/*---------Address--------------------------------------------------------------------------------------*/}
                        <View style={styles.EachProfileRow}>
                            <Text
                                style={[styles.ProfileLabel, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                                آدرس
                            </Text>
                            <TextInput
                                onChangeText={(x) => setAddress(x)}
                                style={[styles.ProfileInput]}
                                value={address}
                                // multiline={true}
                                placeholderTextColor={'#ddd'}
                            />
                        </View>
                        <TouchableOpacity
                            style={[styles.UpdateProfileBtn, {backgroundColor: context.theme != 'dark' ? '#333' : '#3498db'}]}
                            onPress={UpdateProfile}>
                            {
                                loading ?
                                    <ActivityIndicator size={25} color={'#fff'}/> :
                                    <Text style={[styles.UpdateProfileLabel]}> ثبت تغییرات </Text>
                            }
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            )}
        </Context.Consumer>
    );

};



