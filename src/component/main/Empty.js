import React, {} from 'react';
import {Text, StyleSheet} from 'react-native';
import {Context} from '../../context/context';


export default function Empty(props) {
    return (
        <Context.Consumer>
            {(context) => (
                <Text
                    style={[styles.EmptyTile, {color: context.theme != 'dark' ? '#333' : '#fff'}]}>{props.Message}</Text>

            )}
        </Context.Consumer>
    );
};




const styles=StyleSheet.create({
    EmptyTile:{
        fontFamily:'BYekan',
        textAlign:'center',
    }
})