import React, {useState, useContext} from 'react';
import {
    TouchableOpacity,
    View,
    ActivityIndicator,
    AsyncStorage,
    Text,
    TextInput,
    StatusBar,
    ScrollView
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Login';
import Icon from "react-native-vector-icons/MaterialIcons";
import Styles, {DarkBG, DarkText, LightText, BtnRadius} from "../../assets/css/Styles";
import Host from "../../core/Host";
import Connect from "../../core/Connect";

export default function Login({navigation}) {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [hidePass, HidePass] = useState(true);
    const [loading, setLoading] = useState(false);

    const context = useContext(Context);
    //-----------Login Fn----------------------------------------------------------------------------------------------
    const Login = () => {
        if (username && password) {
            setLoading(true)
            Connect.PostReq(Host.Link('login'), {
                username: username,
                password: password,
            }).then(res => {
                setLoading(false);
                // console.warn(res.role);
                if (res.result) {
                    AsyncStorage.multiSet([['id', (res.id).toString()], ['role', res.role]]),
                        context.setRole(res.role);
                    context.setUsername(username);
                    if (res.role == 'user') {
                        navigation.replace('Main')
                    } else if (res.role == 'moshaver') {
                        navigation.replace('OldChatList')
                    }
                } else {
                    alert(res.message)
                }
            }).catch(() => {
                setLoading(false);
                alert('ورود انجام نشد لطفا دوباره تلاش کنید')
            });
        } else {
            alert('نام کاربری و رمز را وارد کنید')
        }
    };
    return (
        <Context.Consumer>
            {(context) => (
                <ScrollView contentContainerStyle={[styles.LoginContainer,]}
                            style={{backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}}
                            showsVerticalScrollIndicator={false}>
                    <StatusBar backgroundColor={context.theme == 'dark' ? "#333" : "#fff"}
                               barStyle={context.theme != 'dark' ? "dark-content" : "light-content"}/>
                    {/*---------username--------------------------------------------------------------------------------------*/}
                    <View style={styles.LoginEeachRow}>
                        <TextInput
                            onChangeText={(x) => setUsername(x)}
                            style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                            value={username}
                            placeholder={'نام کاربری'}
                            placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            blurOnSubmit={false}
                        />
                    </View>

                    {/*---------Password--------------------------------------------------------------------------------------*/}
                    <View style={styles.LoginEeachRow}>
                        <View style={styles.passwordInp}>
                            <Icon name={'visibility'} size={20} color={context.theme == 'dark' ? DarkText : LightText}
                                  onPress={() => HidePass(!hidePass)}/>
                            <TextInput
                                onChangeText={(x) => setPassword(x)}
                                style={[styles.SignupInput, {
                                    width: '93%',
                                    color: context.theme == 'dark' ? DarkText : LightText
                                }]}
                                value={password}
                                secureTextEntry={hidePass}
                                placeholder={'رمز عبور'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>
                    </View>
                    <TouchableOpacity
                        style={[styles.LoginBtn, {backgroundColor: context.theme != 'dark' ? '#333' : '#3498db'}]}
                        onPress={Login}>
                        {
                            loading ?
                                <ActivityIndicator size={25} color={'#fff'}/> :
                                <Text style={[styles.LoginBtnLabel]}>ورود</Text>
                        }
                    </TouchableOpacity>
                    <Text onPress={() => navigation.navigate('SingUp')}
                          style={[styles.SignupBtn, {
                              color: context.theme == 'dark' ? DarkText : LightText,
                              borderColor: context.theme == 'dark' ? DarkText : LightText
                          }]}>ثبت نام</Text>
                </ScrollView>
            )}
        </Context.Consumer>
    );
};



