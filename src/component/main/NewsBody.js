import React from 'react';
import {
    View, Text, ScrollView, Image, TouchableOpacity
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/NewsBody';
import Header from "./Header";
import Host from "../../core/Host";


export default function NewsBody(props) {
    // console.warn()
    let item = props.navigation.getParam('news');
    return (
        <Context.Consumer>
            {(context) => (
                <ScrollView
                    style={[styles.NewsBodyContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={props.navigation}/>
                    {
                        item.image ?
                            <Image source={{uri: Host.Media(item.image)}}
                                   style={styles.NewsBigLogo}/>
                            :
                            <Image source={require('./../../assets/Images/newsLogo.png')} style={styles.NewsLogo}/>
                    }
                    <Text
                        style={[styles.NewsBody, {color: context.theme != 'dark' ? '#333' : '#fff'}]}>{item.body}</Text>
                </ScrollView>
            )}
        </Context.Consumer>
    );
};



