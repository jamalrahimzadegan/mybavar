import React, { useState } from 'react';
import { View, StatusBar, } from 'react-native';
import { Context } from '../../context/context';
import styles from '../../assets/css/Header';
import Icon from "react-native-vector-icons/Ionicons";
import {NavigationActions, StackActions} from "react-navigation";


function Header({ navigation }) {
    function NavHome() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Main'})],
        });
        navigation.dispatch(resetAction);
    }

    return (
        <Context.Consumer>
            {(context) => (
                <View style={styles.HeaderContainer}>
                    <StatusBar backgroundColor={context.theme == 'dark' ? "#333" : "#fff"}
                        barStyle={context.theme != 'dark' ? "dark-content" : "light-content"} />
                    <View style={styles.HeaderIconsContainer}>
                        <Icon name={"md-arrow-round-back"} color={context.theme !== 'dark' ? "#333" : "#fff"} size={30}
                            onPress={() => navigation.goBack()} />
                        {
                            context.role=='user'?
                            <Icon name={"ios-home"} color={context.theme !== 'dark' ? "#333" : "#fff"} size={30}
                                onPress={() => NavHome()} />:null
                        }

                        <Icon name={"ios-menu"} color={context.theme !== 'dark' ? "#333" : "#fff"} size={30}
                            onPress={() => navigation.openDrawer()} />
                    </View>
                </View>
            )}
        </Context.Consumer>
    );
}

export default Header;
