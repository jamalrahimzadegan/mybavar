import React, {useEffect, useState, useContext} from 'react';
import {Text, View, TouchableOpacity, Image, FlatList, Alert, AsyncStorage,} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Reminders';
import Header from "./Header";
import Connect from "../../core/Connect";
import Host from "../../core/Host";
import Empty from "./Empty";
import RenderReminders from "../Flatlist/RenderReminders";
import Icon from "react-native-vector-icons/AntDesign";

var PushNotification = require("react-native-push-notification");


export default function Reminders({navigation}) {
    PushNotification.cancelAllLocalNotifications();
    const [reminders, setReminders] = useState([]);
    let context = useContext(Context);
    //----------Fetch Reminders fn---------------------------------------------------------------------------------------
    const GetReminders = (id) => {
        Connect.PostReq(Host.Link('allreminder'), {
            userId: parseInt(id)
        }).then(res => {
            setReminders(res.allrem);
        }).catch((e) => null);
    };
    useEffect(() => {
        navigation.addListener('didFocus', () => {
            GetReminders(context.id);
        });
        if (context.id == undefined) {
        } else {
            GetReminders(context.id);
        }
    }, []);

    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.ReminderContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={navigation}/>
                    <FlatList
                        // data={[{
                        //     'id': '1',
                        //     'time': '14:56',
                        //     'text': 'first noti',
                        //     'date': '2020-03-12'
                        // },
                        //     // {
                        //     //     'id': '2',
                        //     //     'time': '13:33',
                        //     //     'text': 'sec noti',
                        //     //     'date': '2020-03-12'
                        //     // }
                        // ]}
                        data={reminders}
                        ListEmptyComponent={() => <Empty Message={'یادآوری وجود وجود ندارد'}/>}
                        keyExtractor={(item, index) => index}
                        renderItem={(item) => <RenderReminders GetReminders={GetReminders}
                                                               item={item} navigation={navigation}/>}
                    />
                    <Icon style={styles.AddRemBtn} onPress={() => navigation.navigate('CreateReminder', {
                        GetReminders: () => props.GetReminders(id),
                    })} size={35}
                          color={'#fff'} name={'pluscircleo'}/>
                </View>
            )}
        </Context.Consumer>
    );

};




