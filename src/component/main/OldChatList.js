import React, {useState, useContext, useEffect} from 'react';
import {View,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/ChatList';
import Host from "../../core/Host";
import Connect from "../../core/Connect";
import Header from "./Header";
import RenderChatList from '../Flatlist/RenderOldChatsList';
import Empty from "./Empty";



export default function OldChatList(props) {
    const [Chats, setChats] = useState([]);
    const context = useContext(Context);
    //---------------------------------------------------------------------------------------------------------------------------------------
    const GetChatList = () => {
        Connect.PostReq(Host.Link('alltickets'), {
            id: parseInt(context.id)
        }).then(res => {
            setChats(res.tickets);
        }).catch((e) => {
        });
    };
    //------------province render --------------------------------------------------------------------------------------------------------------------------
    useEffect(() => {
        GetChatList()
    }, [context]);
    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.OldChatContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={props.navigation}/>
                    {
                        Chats.length == 0 ?
                            <ActivityIndicator size={30} style={{margin: 20}}/>
                            :
                            <FlatList
                                data={Chats}
                                ListEmptyComponent={() => <Empty Message={'گفتگویی وجود ندارد'}/>}
                                keyExtractor={(item, index) => index}
                                renderItem={(item) => <RenderChatList GetChatList={GetChatList} item={item}
                                                                      navigation={props.navigation}/>}
                            />
                    }
                </View>
            )}
        </Context.Consumer>
    );
};