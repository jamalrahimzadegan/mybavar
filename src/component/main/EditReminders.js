import React, {useEffect, useContext, useState} from 'react';
import {
    View, Text, ScrollView, ActivityIndicator, TouchableOpacity, TextInput
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Reminders';
import Header from "./Header";
import Host from "../../core/Host";
import Connect from "../../core/Connect";
import {DarkText, LightText} from "../../assets/css/Styles";

var PushNotification = require("react-native-push-notification");


export default function EditReminders({navigation}) {

    let context = useContext(Context);
    let item = navigation.getParam('item');
    const [reminderTitle, setReminderTitle] = useState(item.text);
    const [Loading, setLoading] = useState(false);
    const [Hour, setHour] = useState(item.time.slice(0, 2));
    const [Min, setMin] = useState(item.time.slice(3, 5));
    let TextColor = context.theme == 'dark' ? DarkText : LightText;
    let BackColor = context.theme == 'dark' ? '#333' : '#fff';
    let BorderColor = context.theme != 'dark' ? '#333' : '#fff';
    //------------TOday date-----------------------------
    const TodayDate = () => {
        var d = new Date(Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, parseInt(day) + 1].join('-');
    };
    //----------Update Reminders--------------------------------------------------------------------------------------------
    const UpdateReminder = () => {
        setLoading(true)
        console.warn(item)
        Connect.PostReq(Host.Link('updatereminder'), {
            userId: parseInt(context.id),
            id: parseInt(item.id),
            text: reminderTitle,
            time: Hour + ':' + Min,
            date: TodayDate(),
        }).then(res => {
            setLoading(false)
            console.warn(res);
            if (res.result) {
                PushNotification.cancelAllLocalNotifications();
                alert('با موفقیت ویرایش شد')
                setTimeout(() => navigation.goBack(), 2000)
            }
        }).catch((e) => {
            setLoading(false);
            console.warn(e)
        });
    };
    //----------Time Input Check--------------------------------------------------------------------------------------------
    const setTime = (time, type) => {
        if (type == 'h' && time <= 23) {
            setHour(time)
        } else if (type == 'm' && time <= 59) {
            setMin(time)
        } else {
            alert('زمان را به درستی وارد کنید')
        }
    }

    return (
        <Context.Consumer>
            {(context) => (
                <ScrollView
                    style={[styles.ReminderContainer, {backgroundColor: BackColor}]}>
                    <Header navigation={navigation}/>
                    <Text style={[styles.ReminderTime, {color: TextColor}]}>متن یادآور:</Text>
                    <TextInput
                        onChangeText={(x) => setReminderTitle(x)}
                        style={[styles.ReminderTitleInp, {color: TextColor, borderColor: BorderColor}]}
                        value={reminderTitle}
                        placeholder={'متن یادآور'}
                        placeholderTextColor={TextColor}
                        blurOnSubmit={false}
                    />
                    <Text style={[styles.ReminderTime, {color: TextColor}]}>زمان: </Text>
                    <View style={styles.EditReminderTime}>
                        <TextInput
                            keyboardType={'numeric'}
                            maxLength={2}
                            onChangeText={(h) => setTime(h, 'h')}
                            style={[styles.ReminderTitleInp, {
                                width: '49%',
                                color: TextColor,
                                borderColor: BorderColor
                            }]}
                            value={Hour}
                            placeholder={'ساعت'}
                            placeholderTextColor={TextColor}
                            blurOnSubmit={false}
                        />
                        <TextInput
                            keyboardType={'numeric'}
                            maxLength={2}
                            onChangeText={(m) => setTime(m, 'm')}
                            style={[styles.ReminderTitleInp, {
                                width: '49%',
                                color: TextColor,
                                borderColor: BorderColor
                            }]}
                            value={Min}
                            placeholder={'دقیقه'}
                            placeholderTextColor={TextColor}
                            blurOnSubmit={false}
                        />
                    </View>
                    <TouchableOpacity style={styles.RemUpBtn} onPress={UpdateReminder}>
                        {
                            Loading ?
                                <ActivityIndicator color={'#fff'} size={25}/> :
                                <Text style={styles.ReminderTitle}>بروزرسانی</Text>

                        }
                    </TouchableOpacity>
                </ScrollView>
            )}
        </Context.Consumer>
    );
};




