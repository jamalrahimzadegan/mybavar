import React, {useState, useEffect} from 'react';
import {
    View, Text, ActivityIndicator, StatusBar, TouchableOpacity, Button
} from 'react-native';
import styles from '../../assets/css/Player';
import {Context} from '../../context/context';
import Host from '../../core/Host';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import {BoxBG, DarkBG} from "../../assets/css/Styles";


export default function Player({navigation}) {
    const [pause, setPause] = useState(false);
    const [duration, setDuration] = useState(0);
    const [curTime, setCurrentTime] = useState(0);
    const [HideControls, HideControlBar] = useState(false);
    let File = navigation.getParam('fileName').file;
    let Type = navigation.getParam('Type');
    let Poster = navigation.getParam('fileName').img;
    //-----------Seek the Seekbar--------------------------------------------------------------------------------------
    const Seek = (SeekType) => {
        if (SeekType == 'backward') {
            player.seek(curTime - 10);
        } else if (SeekType == 'forward') {
            player.seek(curTime + 10);
        }
    };
    //-----------formating duration and current time in mm:ss --------------------------------------------------------------------------------------
    const formatTime = (seconds) => {
        let minutes = Math.floor(seconds / 60);
        minutes = (minutes >= 10) ? minutes : "0" + minutes;
        seconds = Math.floor(seconds % 60);
        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        return minutes + ":" + seconds;
    }
    useEffect(() => {
        if (navigation.state.routeName == 'Main') {
            console.warn(navigation.state.routeName)
        }
    }, []);
    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.PlayerContainer,]}>
                    <StatusBar hidden={true}/>
                    {
                        Type == 'mp3' ? null :
                            <Icon style={styles.HideCtrlBtn} name={'keyboard-arrow-down'}
                                  onPress={() => {
                                      HideControlBar(!HideControls);
                                  }} color={'#fff'} size={27}/>
                    }
                    <Video
                        source={{uri: Host.Media(File)}}
                        ref={(ref) => player = ref}
                        resizeMode={'contain'}
                        controls={false}
                        playInBackground={false}
                        repeat={true}
                        volume={.4}
                        onEnd={() => {
                            setPause(true);
                            setCurrentTime(0);
                            navigation.replace('Main');
                        }}
                        paused={pause}
                        poster={Type == 'mp3' ? Host.Media(Poster) : ''}
                        posterResizeMode={'contain'}
                        audioOnly={Type == 'mp3'}
                        currentTime={5}
                        onLoad={(x) => setDuration(parseInt(x.duration))}
                        onProgress={(x) => setCurrentTime(parseInt(x.currentTime))}
                        fullscreenOrientation={'landscape'}
                        style={{flex: 1}}/>
                    {
                        HideControls ? null :
                            <View style={[styles.CtrlBarContainer, {}]}>
                                <Text style={styles.TimesFont}>{File.slice(0, File.length - 4)}</Text>
                                <View style={styles.PlayerControls}>
                                    <Text style={styles.TimesFont}>{formatTime(curTime)}</Text>
                                    {/*------------backward BTN------------------------*/}
                                    <TouchableOpacity style={styles.CtrlBtnContainer}>
                                        <Icon name={'fast-rewind'} size={27}
                                              onPress={() => Seek('backward')} color={'#fff'}/>
                                    </TouchableOpacity>
                                    {/*------------play pause BTN------------------------*/}
                                    <TouchableOpacity style={styles.CtrlBtnContainer} onPress={() => setPause(!pause)}>
                                        {
                                            pause ?
                                                <Icon1 style={styles.PlayrIons} name={'play'} color={'#ff'} size={30}/>
                                                :
                                                <Icon1 style={styles.PlayrIons} name={'pause'} color={'#fff'}
                                                       size={30}/>
                                        }
                                    </TouchableOpacity>
                                    {/*------------forward BTN------------------------*/}
                                    <TouchableOpacity style={styles.CtrlBtnContainer}>
                                        <Icon style={styles.PlayrIons} name={'fast-forward'} size={27}
                                              onPress={() => Seek('forward')} color={'#fff'}/>
                                    </TouchableOpacity>
                                    <Text style={[styles.TimesFont]}>{formatTime(duration)}</Text>
                                </View>
                                <View style={styles.SeekBarContainer}>
                                    <View
                                        style={[styles.SeekBar, {width: duration ? `${(curTime * 100) / duration}%` : 0}]}/>
                                </View>
                            </View>
                    }
                    {
                        duration == 0 ?
                            <ActivityIndicator size={35} color={BoxBG} style={styles.Loader}/> : null
                    }
                </View>
            )}
        </Context.Consumer>
    );
};




