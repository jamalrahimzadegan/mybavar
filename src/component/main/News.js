import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, FlatList, StatusBar, ScrollView } from 'react-native';
import { Context } from '../../context/context';
import styles from '../../assets/css/News';
import Header from "./Header";
import Connect from "../../core/Connect";
import Host from "../../core/Host";
import RenderNews from "../Flatlist/RenderNews";

import Empty from "./Empty";

export default function News({ navigation }) {
    const [news, setNews] = useState([]);
    //----------news fn---------------------------------------------------------------------------------------
    const GetNews = () => {
        Connect.PostReq(Host.Link('news'), {})
            .then(res => {
                // console.warn(res.news)
                setNews(res.news);
            }).catch((e) => null);
    };
    useEffect(() => {
        GetNews()
    }, []);

    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.MainContainer, { backgroundColor: context.theme == 'dark' ? '#333' : '#fff' }]}>
                    <Header navigation={navigation} />
                    <Text  style={[styles.NewsTitle, { color: context.theme != 'dark' ? '#333' : '#fff' }]}>جدیدترین خبرها</Text>
                    <FlatList
                            style={{ margin: 8 }}
                            data={news}
                            ListEmptyComponent={()=><Empty Message={'خبری وجود ندارد'}/>}
                            keyExtractor={(item, index) => index}
                            renderItem={(item) => <RenderNews item={item} navigation={navigation} />}
                        />

                </View>
            )}
        </Context.Consumer>
    );

};




