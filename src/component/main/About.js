import React, {useEffect, useState} from 'react';
import {
    View, Text, ScrollView, Image, TouchableOpacity
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/About';
import Header from "./Header";
import Host from "../../core/Host";
import Connect from "../../core/Connect";


export default function About({navigation}) {
    // console.warn()
    const [about, setAbout] = useState();
    //----------news fn---------------------------------------------------------------------------------------
    const GetAbout = () => {
        Connect.PostReq(Host.Link('about'), {})
            .then(res => {
                // console.warn(res.about.body)
                setAbout(res.about.body);
            }).catch((e) => null);
    };
    useEffect(() => {
        GetAbout()
    }, []);
    return (
        <Context.Consumer>
            {(context) => (
                <ScrollView
                    style={[styles.AboutContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={navigation}/>
                    <Text style={[styles.AboutTitle, {color: context.theme != 'dark' ? '#333' : '#fff'}]}>درباره
                        ما</Text>
                    <Text style={[styles.AboutBody, {color: context.theme != 'dark' ? '#333' : '#fff'}]}>{about}</Text>
                </ScrollView>
            )}
        </Context.Consumer>
    );
};




