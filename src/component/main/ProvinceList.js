import React, {useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import {Context} from '../../context/context';
import styles from '../../assets/css/Chat';
import Host from "../../core/Host";
import Connect from "../../core/Connect";
import Header from "./Header";


export default function ProvinceList({navigation}) {
    const [province, setProvinces] = useState([]);
    //--------------------------------------------------------------------------------------------------------------------------------------
    const GetProvinceList = () => {
        Connect.PostReq(Host.Link('province'), {})
            .then(res => {
                setProvinces(res.province)
            }).catch((e) => {
        });
    };
    //---------------------------------------------------------------------------------------------------------------------------------------
    const Provinces = (item) => {
        let Item = item.item.item;
        // console.warn(item.item.item.name)
        return (
            <TouchableOpacity onPress={() => navigation.navigate('ChatPeers', {ProID: Item.id})}
                              style={styles.ProvinceBtn}>
                <Text style={styles.MessageText}>{Item.name}</Text>
            </TouchableOpacity>
        )
    }
    //------------province render --------------------------------------------------------------------------------------------------------------------------
    useEffect(() => {
        GetProvinceList()
    }, []);

    return (
        <Context.Consumer>
            {(context) => (
                <View
                    style={[styles.ProvinceListContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header navigation={navigation}/>
                    {
                        province.length !== 0 ?
                            <FlatList
                                style={styles.ChatContainer}
                                data={province}
                                keyExtractor={(item, index) => index}
                                renderItem={(item) => <Provinces item={item} navigation={navigation}/>}
                            /> : <ActivityIndicator size={30} style={{margin: 20}}/>
                    }
                </View>
            )}
        </Context.Consumer>
    );

};




