import React, {useEffect, useState, useContext} from 'react';
import {ImageBackground, View, TouchableOpacity, Image, FlatList, Dimensions,} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/Main';
import Header from "./Header";
import Connect from "../../core/Connect";
import Host from "../../core/Host";
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import OldChatList from './OldChatList';
import RenderFiles from './../Flatlist/RenderFiles';
import Empty from "./Empty";

const H = Dimensions.get('window').height;
const W = Dimensions.get('window').width;

var PushNotification = require("react-native-push-notification");


export default function Main({navigation}) {
    const [files, setFiles] = useState([]);
    //----------GetFiles fn---------------------------------------------------------------------------------------
    const GetFiles = () => {
        Connect.PostReq(Host.Link('file'), {})
            .then(res => {
                setFiles(res.files);
            }).catch((e) => null);
    };
    useEffect(() => {
        GetFiles();
    }, []);

    return (
        <Context.Consumer>
            {(context) => (
                <View style={[styles.MainContainer, {backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}]}>
                    <Header onPress={console.warn(context.username)} navigation={navigation}/>
                    {
                        H > W ?
                            <ImageBackground blurRadius={2} source={require('./../../assets/Images/MainPageCover.jpg')}
                                             style={styles.MainPageCover}>
                                <Image source={require('./../../assets/Images/MainPageLogo.png')}
                                       style={styles.MainPageLogo}/>
                                <View style={styles.MainPageBtnContainer}>
                                    <TouchableOpacity style={styles.NewChatBtn}
                                                      onPress={() => navigation.navigate('OldChatList')}>
                                        <Icon1 name={'chat'} color={'#fff'} size={29}/>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navigation.navigate('ProvinceList')}
                                                      style={styles.NewChatBtn}>
                                        <Icon name={'pencil'} color={'#fff'} size={29}/>
                                    </TouchableOpacity>
                                </View>
                            </ImageBackground> : null
                    }

                    <FlatList
                        data={files}
                        ListEmptyComponent={() => <Empty Message={'موردی وجود وجود ندارد'}/>}
                        keyExtractor={(item, index) => index}
                        renderItem={(item) => <RenderFiles item={item} navigation={navigation}/>} // فایل انگیزشی
                    />
                </View>
            )}
        </Context.Consumer>
    );

};




