import React, {useEffect, useState} from 'react';
import {
    View, StatusBar, ActivityIndicator
} from 'react-native';
import {Context} from '../../context/context';
import PDFView from 'react-native-view-pdf';
import Host from './../../core/Host';


export default function ShowPdf({navigation}) {
    const [loading, setLoading] = useState(true)
    return (
        <Context.Consumer>
            {(context) => (
                <View style={{flex: 1, backgroundColor: '#fff'}}>
                    <StatusBar hidden={true}/>
                    {
                        loading ?
                            <ActivityIndicator style={{margin: 20}} size={30}/> : null

                    }
                    <PDFView
                        onLoad={() => setLoading(false)}
                        onError={() => {
                            setLoading(false);
                            alert('خطایی رخ داده است، لطفا دوباره تلاش کنید')
                            navigation.replace('Main')
                        }}
                        style={{flex: 1, width: '100%', backgroundColor: '#fff'}}
                        textEncoding={"utf-8"}
                        // resource={'http://nog-et.com/upload/1582016755.pdf'}
                        resource={Host.Media(navigation.getParam('PdfName').file)}
                        resourceType={"url"}
                    />
                </View>
            )}
        </Context.Consumer>
    );
};




