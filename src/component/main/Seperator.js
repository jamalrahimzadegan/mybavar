import React, {useState} from 'react';
import {View} from 'react-native';
import {Context} from '../../context/context';
import {DarkBG, LightBG} from "../../assets/css/Styles";


function Seperator(props) {
    return (
        <Context.Consumer>
            {(context) => (
                <View style={{
                    width: props.Width ? props.Width : '100%',
                    height: props.Height ? props.Height : 1.7,
                    backgroundColor: context.theme !== 'dark' ? DarkBG : LightBG,
                    opacity: .4,
                    marginVertical:props.MrginVertical?props.MrginVertical:5,
                }}/>
            )}
        </Context.Consumer>
    );
}

export default Seperator;
