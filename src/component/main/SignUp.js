import React, {useState} from 'react';
import {
    TouchableOpacity,
    View,
    ActivityIndicator,
    AsyncStorage,
    Text,
    TextInput,
    StatusBar,
    ScrollView
} from 'react-native';
import {Context} from '../../context/context';
import styles from './../../assets/css/SignUp';
import Icon from "react-native-vector-icons/MaterialIcons";
import Styles, {DarkBG, DarkText, LightText} from "../../assets/css/Styles";
import Host from "../../core/Host";
import Connect from "../../core/Connect";

export default function SingUp({navigation}) {
    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [hidePass, HidePass] = useState(true);
    const [mobile, setMobile] = useState();
    const [fullname, setFullname] = useState();
    const [address, setAddress] = useState();
    const [loading, setLoading] = useState(false);
    //-----------Singup Fn--------------------------------------------------------------------------------------
    const Signup = () => {
        if (
            username,
                email,
                password,
                fullname,
                mobile,
                address
        ) {
            setLoading(true)
            Connect.PostReq(Host.Link('signup'), {
                username: username,
                email: email,
                password: password,
                fullname: fullname,
                mobile: mobile,
                address: address
            }).then(res => {
                setLoading(false);
                if (res.result) {
                    setUsername(username)
                    // console.warn(res)
                    AsyncStorage.setItem('id', res.id.toString());
                    alert('ثبت نام با موفقیت انجام شد')
                    navigation.replace('Main')
                }
            }).catch((e) => {
                setLoading(false);
                alert('ثبت نام انجام نشد لطفا دوباره تلاش کنید')
            });
        } else {
            alert('لطفا همه موارد را به درستی وارد کنید')
        }
    };
    return (
        <Context.Consumer>
            {(context) => (
                <View style={{flex: 1}}>
                    <ScrollView contentContainerStyle={[styles.SignupContainer]}
                                style={{backgroundColor: context.theme == 'dark' ? '#333' : '#fff'}}
                                showsVerticalScrollIndicator={false}>
                        <StatusBar backgroundColor={context.theme == 'dark' ? "#333" : "#fff"}
                                   barStyle={context.theme != 'dark' ? "dark-content" : "light-content"}/>
                        {/*---------Fullname--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <TextInput
                                onChangeText={(x) => setFullname(x)}
                                style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                                value={fullname}
                                placeholder={'نام کامل'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>
                        {/*---------username--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <TextInput
                                onChangeText={(x) => setUsername(x)}
                                style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                                value={username}
                                placeholder={'نام کاربری'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>

                        {/*---------Password--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <View style={styles.passwordInp}>
                                <Icon name={'visibility'} size={20}
                                      color={context.theme == 'dark' ? DarkText : LightText}
                                      onPress={() => HidePass(!hidePass)}/>
                                <TextInput
                                    onChangeText={(x) => setPassword(x)}
                                    style={[styles.SignupInput, {
                                        width: '93%',
                                        color: context.theme == 'dark' ? DarkText : LightText
                                    }]}
                                    value={password}
                                    secureTextEntry={hidePass}
                                    placeholder={'رمز عبور'}
                                    placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                                />
                            </View>
                        </View>
                        {/*---------email--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <TextInput
                                keyboardType={'email-address'}
                                onChangeText={(x) => setEmail(x)}
                                style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                                value={email}
                                placeholder={'ایمیل'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>
                        {/*---------mobile--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <TextInput
                                keyboardType={'numeric'}
                                maxLength={11}
                                onChangeText={(x) => setMobile(x)}
                                style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                                value={mobile}
                                placeholder={'شماره موبایل'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>
                        {/*---------Address--------------------------------------------------------------------------------------*/}
                        <View style={styles.SignupEeachRow}>
                            <TextInput
                                onChangeText={(x) => setAddress(x)}
                                style={[styles.SignupInput, {color: context.theme == 'dark' ? DarkText : LightText}]}
                                value={address}
                                // multiline={true}
                                placeholder={'آدرس'}
                                placeholderTextColor={context.theme == 'dark' ? DarkText : LightText}
                            />
                        </View>
                        <TouchableOpacity
                            style={[styles.SignupBtn, {backgroundColor: context.theme != 'dark' ? '#333' : '#3498db'}]}
                            onPress={Signup}>
                            {
                                loading ?
                                    <ActivityIndicator size={25} color={'#fff'}/> :
                                    <Text style={[styles.SingupBtnLabel]}> ثبت نام </Text>
                            }
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            )}
        </Context.Consumer>
    )
};




