import React, {useEffect, useContext, useState} from 'react';
import {View, TouchableOpacity, Text, ScrollView, Alert, Button, Image, AsyncStorage} from 'react-native';
import styles from './../assets/css/DrawerStyle';
import {Context} from '../context/context';
import {DarkBG, DarkText, LightBG, LightText} from '../assets/css/Styles';
import Seperator from "./main/Seperator";
import {NavigationActions, StackActions} from 'react-navigation';

export default function DrawerStyle({navigation}) {


    return (
        <Context.Consumer>
            {(context) => (
                <ScrollView contentContainerStyle={styles.MainView2}
                            style={[styles.MainView, {backgroundColor: context.theme == 'dark' ? DarkBG : LightBG}]}
                            showsVerticalScrollIndicator={false}>
                    {
                        0 ?
                            <View style={styles.DrawerTopSegment}>
                                <Image source={require('./../assets/Images/avatar.png')} style={styles.Avatar}/>
                                <View style={styles.DrawerNameToggleBtn}>
                                    <Text
                                          style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>{context.username}</Text>

                                </View>
                            </View> : null
                    }
                    {
                        0 ?
                            <Seperator Height={1.65} MrginVertical={15}/> : null
                    }
                    {
                        context.role == 'user' ?
                            <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.DrawerRow}>
                                <Text
                                    style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>■
                                    پروفایل</Text>
                            </TouchableOpacity>
                            : null
                    }
                    {
                        context.role == 'user' ?
                            <Seperator Height={.75} MrginVertical={-1}/> : null
                    }
                    <TouchableOpacity onPress={() => navigation.navigate('News')} style={styles.DrawerRow}>
                        <Text
                            style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>
                            ■ اخبار</Text>
                    </TouchableOpacity>
                    <Seperator Height={.75} MrginVertical={-1}/>

                    <TouchableOpacity onPress={() => navigation.navigate('Reminders')} style={styles.DrawerRow}>
                        <Text
                            style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>■
                            یادآور</Text>
                    </TouchableOpacity>
                    <Seperator Height={.75} MrginVertical={-1}/>

                    <TouchableOpacity onPress={() => DarkMode(context)}
                                      style={[styles.DrawerRow, {}]}>
                        <Text
                            style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>{context.theme === 'light' ? '■  حالت شب' : '■  حالت روز'}</Text>
                    </TouchableOpacity>
                    <Seperator Height={.75} MrginVertical={-1}/>
                    <TouchableOpacity onPress={() => navigation.navigate('About')} style={styles.DrawerRow}>
                        <Text style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>■
                            درباره
                            ما</Text>
                    </TouchableOpacity>
                    <Seperator Height={.75} MrginVertical={-1}/>
                    <TouchableOpacity onPress={() => Logout(context)} style={styles.DrawerRow}>
                        <Text
                            style={[styles.ToggleText, {color: context.theme == 'dark' ? DarkText : LightText}]}>■
                            خروج</Text>
                    </TouchableOpacity>
                    <Seperator Height={.75} MrginVertical={-1}/>
                    <Image source={require('./../assets/Images/Logo.png')} style={styles.DrawerLogoApp}/>
                </ScrollView>
            )
            }
        </Context.Consumer>
    );
    function DarkMode(context) {
        if (context.theme === 'light') {
            AsyncStorage.setItem('theme', 'dark');
            context.setTheme('dark');
        } else {
            AsyncStorage.setItem('theme', 'light');
            context.setTheme('light');
        }
    }

    function Logout(context) {
        Alert.alert(
            '', 'آیا مایل به خروج از حساب کاربری خود می باشید؟',
            [
                {
                    text: 'خیر',
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: 'بله', onPress: () => {
                        AsyncStorage.removeItem('id');
                        AsyncStorage.removeItem('role');
                        context.setId('logout');
                        context.setUsername();
                        context.setRole();
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({routeName: 'Splash'})],
                        });
                        navigation.dispatch(resetAction);
                    }
                },
            ],
            {cancelable: false},
        );

    }

};
