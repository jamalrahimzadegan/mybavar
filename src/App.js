import React, {Component, useEffect} from 'react';
import {AsyncStorage} from 'react-native';
import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import Splash from './component/Splash';
import {ContextProvider} from './context/context';
import DrawerStyle from './component/DrawerStyle';
import Main from "./component/main/Main";
import SingUp from "./component/main/SignUp";
import Chat from "./component/main/Chat";
import Login from "./component/main/Login";
import NewsBody from "./component/main/NewsBody";
import About from "./component/main/About";
import Profile from "./component/main/Profile";
import ChatPeers from "./component/main/ChatPeers";
import ProvinceList from "./component/main/ProvinceList";
import OldChatList from './component/main/OldChatList';
import News from './component/main/News';
import ShowPdf from './component/main/ShowPdf';
import Player from './component/main/Player';
import Reminders from './component/main/Reminders';
import EditReminders from "./component/main/EditReminders";
import CreateReminder from "./component/main/CreateReminder";

var PushNotification = require("react-native-push-notification");


const AppNavigator = createStackNavigator({
    Splash: {screen: Splash, navigationOptions: {header: null}},
    // ----Files section-----------------------------------------------------------
    Main: {screen: Main, navigationOptions: {header: null}},
    ShowPdf: {screen: ShowPdf, navigationOptions: {header: null}},
    Player: {screen: Player, navigationOptions: {header: null}},
    // --------------------------------------------------------------------
    News: {screen: News, navigationOptions: {header: null}},
    SingUp: {screen: SingUp, navigationOptions: {header: null}},
    Login: {screen: Login, navigationOptions: {header: null}},
    // -----Chats-----------------------------------------------------------------------
    ProvinceList: {screen: ProvinceList, navigationOptions: {header: null}},
    OldChatList: {screen: OldChatList, navigationOptions: {header: null}},
    ChatPeers: {screen: ChatPeers, navigationOptions: {header: null}},
    Chat: {screen: Chat, navigationOptions: {header: null}}, // PV chat page
    // -----Drawer-----------------------------------------------------------------------
    About: {screen: About, navigationOptions: {header: null}},
    Reminders: {screen: Reminders, navigationOptions: {header: null}},
    CreateReminder: {screen: CreateReminder, navigationOptions: {header: null}},
    EditReminders: {screen: EditReminders, navigationOptions: {header: null}},
    NewsBody: {screen: NewsBody, navigationOptions: {header: null}},
    Profile: {screen: Profile, navigationOptions: {header: null}},
}, {
    // initialRouteName: 'Reminders'


});

//---------Drawer Navigator------------------------------------------------------------------------------------
const DrawerNavigator = createDrawerNavigator({
    Home: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 300,
    drawerPosition: 'right',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),
});


export default function App() {
    PushNotification.cancelAllLocalNotifications();

    return (
        <ContextProvider>
            <DrawerNavigator/>
        </ContextProvider>
    )
};


